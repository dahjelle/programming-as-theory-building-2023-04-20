# What is Programming, Anyway?

A review + discussion of Peter Naur's _Programming as theory building_.

-   Introduction
-   I'm not sure I agree with this paper! But I do think it has some useful things to challenge our thinking about our work.

-   Possible definitions and/or aspects of programming
    -   "the process or activity of writing a program"
    -   "provide [a computer] with coded instructions for the automated performance of a task"
    -   "process of designing, writing, testing, debugging, and maintaining the source code of computer programs"
    -   end result is a product to sell
    -   manager: put requirements in, out comes a product
    -   wizards that utter magical incantations to get a computer to do what we want
-   Naur: claims that _theory building_ is an integral part of programming
-   Why does a definition matter?
    -   affects what development techniques actually work in the really world
    -   affects how successful you will be, both in initial creation but also in modifying code
    -   affects how you might manage a team
    -   affects interactions with other programmers
    -   gives some guidance on "will ChatGPT take my job?"

-   What is programming?
    -   Naur's thesis is "…that programming…should be regarded as an activity by which the programmers form or achieve a certain kind of insight, a theory, of the matters at hand."
    -   while all the above definitions are _true_ to some degree or another, Naur would argue that they are missing something integral to programming, that we forget to our detriment
-   Okay, wise guy, what is a _theory_?
    -   a mental model is in Don Normans' _The Design of Everyday Things_
    -   if I show you a picture, you can create a story about this picture — but even if you tell the story to me, you can answer _new_ questions about that story that you didn't even think to tell me about — that you already knew the answer to
    -   the "rest of the story" that seems obvious to you, but not necessarily to anyone else
    -   Naur claims it _cannot_ be fully expressed to someone else, even with extensive documentation
    -   based on Ryle's _The Concept of Mind_:
        -   "having a theory involves being able to deliver lessons or refresher-lessons in it"
        -   something you build up over time
        -   "the results of any kind of systematic inquiry, whether or not these results make up a deductive system"
        -   "a person toiling at building a theory is preparing himself for, among other things, the effortless exposition of the theories which he gets by building them"
        -   "There is a stage at which a thinker has his theory, but has not yet got it perfectly. He is not completely at home in it." "Telling himself his theory is still somewhat toilsome and one of the objects of this toil is to prepare himself for telling it without toil."
        -   "It is a truism to say that the expert who is thoroughly at home with his theory expounds the several elements of it with complete facility; he is not now having to study what to say, or else he could not be described at being thoroughly at home with his theory."
        -   "our theorizing labors do incorporate a lot of soliloquy and colloquy, a lot of calculating and miscalculating on paper and in our heads"
    -   Naur:
        -   "programmer's building up of knowledge of a certain kind, knowledge taken to be basically the programmer's immediate possession, any documentation being an auxiliary, secondary product"
        -   "This is an example of how the full program text and additional documentation is insufficient in conveying to even the highly motivated group B the deeper insight into the design, that theory which is immediately present to the members of group A."
        -   "Thus, again, the program text and its documentation has proved insufficient as a carrier of some of the most important design ideas."
        -   "Very briefly, a person who has or possesses a theory in this sense knows how to do certain things and in addition can support the actual doing with explanations, justifications, and answers to queries, about the activity of concern."
        -   "…where theory is understood as the knowledge a person must have in order not only to do certain things intelligently but also to explain them, to answer queries about them, to argue about them, and so forth."
-   How does a theory connect with programming?
    -   "…what has to be built by the programmer is a theory of how certain affairs of the world will be handled by, or supported by, a computer program."
    -   "the knowledge possessed by the programmer by virtue of his or her having the theory necessarily, and in an essential manner, transcends that which is recorded in the documented products."
-   "The programmer having the theory of the program can explain how the solution relates to the affairs of the world that it helps to handle." (p. 256)
    -   
-   "The programmer having the theory of the program can explain _why_ each part of the program is what it is…The final basis of the justification is and must always remain the programmer's direct, intuitive knowledge or estimate."
    -   
-   "The programmer having the theory of the program is able to respond constructively to any demand for a modification of the program so as to support the affairs of the world in a new manner."
    -   
-   **If** this theory-building view is correct, what are the implications?
    -   program modification
        -   "…the expectation of the possibility of low cost program modifications conceivably finds support in the fact that a program is a text held in a medium allowing for easy editing. For this support to be valid it must clear be assumed that the dominating cost is one of text manipulation."
        -   "From the insight into the similarity between the new requirements and those already satisfied by the program, the programmer is able to design the change of the program text needed to implement the modification."
    -   program flexibility
    -   program quality
        -   "On the basis of the Theory Building View the decay of a program text as a result of modifications made by programmers without a proper grasp of the underlying theory becomes understandable."
        -   "…a given desired modification may usually be realized in many different ways, all correct. At the same time, if viewed in relation to the theory of the program these ways may look very different, some of them perhaps conforming to that theory or extending it in a natural way, while others may be wholly inconsistent with that theory."
        -   "For a program to retain its quality it is mandatory that each modification is firmly grounded in the theory of it."
-   program life, death, and revival
    -   "A main claim of the Theory Building View of programming is that an essential part of any program ,the theory of it, is something that could not conceivably be expressed, but is inextricably bound to human beings."
    -   "The building of the program is the same os the building of the theory of it by and in the team of programmers."
    -   "During the program life a programmer team possessing its theory remains in active control of the program, and in particular retains control over all modifications."
    -   "The death of a program happens when the programmer team possessing its theory is dissolved. A dead program may continue to be used for execution in a computer and to produce useful results. The actual state of death becomes visible when demands for modifications of the program cannot be intelligently answered."
    -   "Revival of a program is the rebuilding of its theory by a new programmer team."
    -   ideally, you can avoid this process by educating new programmers
        -   similar to educational problem of other activities like writing or a music instrument
        -   "The most important education activity is the student's doing the relevant things under suitable supervision and guidance."
-   "A very important consequence of the Theory Building View is that program revival…is strictly impossible." ‼️
    -   a new team probably would have at least some knowledge of the original theory?
    -   "program revival should only be attempted in exceptional situations and with full awareness that it is at best costly"
    -   "In preference to program revival…the existing program text should be discarded and the new-formed programmer team should be give the opportunity to solve the given problem afresh."
    -   "The point is that building a theory to fit and support an existing program text is a difficult, frustrating, and time consuming activity."
-   "In the Theory Building View what matters most is the building of the theory, while production of documents is secondary.
-   "…the notion of the programmer as an easily replaceable component in the program production activity has to be abandoned"



# Misc.

-   Naur of Backus-Naur fame.
-   "the person having the theory must have an understanding of the manner in which the central laws apply to certain aspects of reality, so as to be able to recognize and apply the theory to other similar aspects."
-   "In the face of a need for a changed manner of operation of the program, one hopes to achieve a saving of costs by making modifications of an existing program text, rather than by writing an entirely new program."
-   ""

-   picture from https://emptysqua.re/blog/programming-as-theory-building/
-   
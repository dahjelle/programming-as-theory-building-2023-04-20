# A Review of "Programming as Theory Building"
## a.k.a., What is programming, anyway?

**These notes**: https://gitlab.com/dahjelle/programming-as-theory-building-2023-04-20/

**Slides**: https://dahjelle.gitlab.io/programming-as-theory-building-2023-04-20/

**Original Paper**: https://algoritmos-iii.github.io/assets/bibliografia/programming-as-theory-building.pdf

---

> **Blurb**: Peter Naur claims that programming is more than just writing code — it is also, and perhaps fundamentally, building up a theory about the program and its relation to the world. What does this mean and how does it affect our day-to-day work as a programmer?
>
> **Bio**: I'm a developer at [Icon Systems, Inc.](https://iconcmo.com), where we build software for churches — keeping track of people, donations, and accounting. I went to school for engineering, but kept getting programming jobs, so here I am.

---

## Slide 1

Hi! My name is David Hjelle. I've been at Icon Systems in Moorhead for nearly 14 years building church software — keeping track of people, donations, and accounting for churches. 

## Slide 2

Welcome to my talk on "Programming as Theory Building: a.k.a. What is programming anyway?".

## Slide 3

I'm doing a review/restatement/trying-to-think-through-the-ideas of a 1985 paper by Peter Naur called "Programming as Theory Building." I originally found out about this paper from the [Future of Coding podcast](https://futureofcoding.org/episodes/061), and I've been grappling with the concepts since. 

As with anything, Naur might be wrong. If Naur was even _partially_ right, there are implications of his ideas that are quite practically useful. We'll talk about just four at the end:

## Slide 4.0

1. when do you rewrite (instead of modify) a bit of code, 

## Slide 4.1

2. how do you best lead a team,

## Slide 4.2

3. usefulness of AI, and

## Slide 4.3

4. compassion.

## Slide 5

Just a brief introduction to Peter Naur. He was a Danish computer scientist that contributed to the Algol programming language (the great-grandfather of the syntax most of our programming languages use) and has his name in Backus-Naur form, the thing that many programming languages use to specify their grammar.

## Slide 6

If you were asked to explain what programming _is_, what would you say?

I can think of a bunch of possible explanations.

## Slide 7

The one that I probably use most often — kind of to fend off that "woah, that must be complicated" reaction when people ask me what I do for a living — is this XKCD.

…wait for audience to read…

## Slide 8

Or you might assume that programming is mostly about the process of typing in a program into a computer, like how this professor seems to expect it to work.

…wait for audience to read…

## Slide 9

Or you might look to the covers of some famous textbooks. Apparently programming is about being a wizard casting magical incantations that mere mortals cannot fathom…

## Slide 10

…or being a mighty, heroic knight slaying dragons!

(Thanks to [Jack Rusher](https://www.youtube.com/watch?v=8Ab3ArE8W3s) and [Robert Nystrom](http://www.craftinginterpreters.com/parsing-expressions.html) for pointing this out.)

## Slide 11

(Some) managers would say that you give a programmer a stack of requirements, and, after a few months, out comes a product to sell.

## Slide 12

You might turn to a dictionary. The "process of designing, writing, testing, debugging and maintaining the source code of computer programs." (That's from Wikipedia.) That seems pretty good, truthfully. 

## Slide 13

Naur would claim that, while all of these may be true, they aren't the full picture, and that they are not the _fundamental_ thing that a programmer spends his or her time doing. Instead, he says, "…programming…should be regarded as an activity by which the programmers form or achieve a certain kind of insight, a theory, of the matters at hand." That's the title of his paper right there: "Programming as Theory Building."

## Slide 14

Okay, well, hmm… But what is a "theory", anyway?

## Slide 15

Naur takes pains to define "theory" in terms of philosopher Gilbert Ryle's book _The Concept of Mind_. (I've read all of 6 pages.)

## Slide 16

Ryle would say that a _theory_ is "the results of any kind of systematic inquiry".

## Slide 17

Once you have it, you are "prepared either to tell it or to apply it, if occasion arises to do so."

## Slide 18

You are "able to deliver lessons or refresher-lessons in it."

## Slide 19

"Building a theory is trying to get a theory, and to have a theory is to have got and not forgotten it. Building a theory is traveling; having a theory is being at one’s destination."

## Slide 20.0

Turning back to Naur, having the "theory" of a program means you can,

1. …explain the connection of the code to the outside world — how does my program meet the use cases that it is intending to solve?

## Slide 20.1

2. …justify _why_ the code is the way it is (versus other possible implementations) in relation to its history and in relation to the problem it is trying to solve

## Slide 20.2

3. …suggest how to modify the program for any change in the real-world scenarios it relates to. If you come to tell me the requirements have changed from X to Y, I can tell you what is going to have to happen in the program.

## Slide 21

This theory is very difficult to build up on your own. It is most effectively transferred from one human being to another. Naur says, "the theory…could not conceivably be expressed, but is inextricably bound to human beings."

## Slide 22

"What is required is that the new programmer has the opportunity to work in close contact with the programmers who already possess the theory."

## Slide 23

Okay, that's the idea of _theory_. I find it most useful to think of this with a couple analogies. I don't have any idea if Ryle or Naur would agree with these in a philosophical sense, but I have found them useful.

## Slide 24

This first analogy is from Donald Norman's book [_The Design of Everyday Things_](https://en.wikipedia.org/wiki/The_Design_of_Everyday_Things). Norman wasn't directly talking about theory building, but about designing user interfaces for products, and the _mental models_ that users form when trying to use something.

## Slide 25

The bottom of this picture shows two dials, the controls for a refrigerator. Notice the instructions for adjusting temperature at the top. Why is this so complicated? Can you figure out what is going on?

…give time for reflection…

## Slide 26

I expect _your_ mental model — your _theory_, so to speak — about what is going on looks like this first diagram: separate cooling units and thermostats for each compartment.

## Slide 27

But, no. It turns out that one dial is a thermostat in the freezer. The _other_ dial is a vent control, controlling airflow between the freezer and the fridge. The correct _mental model_, the correct _theory_ of something is really hard to reverse engineer, even from a set of instructions that are 100% correct.

## Slide 28

Another analogy is the idea of an oracle. I'm not up on my Greek mythology enough to tell you anything about this picture of the Oracle of Delphi, but I _can_ tell you a bit about a black box.

## Slide 29

The idea of a black box is that it does useful computation, and can answer questions that you put to it — but you don't have any idea how it works or what the next answer is going to be.

That's part of the idea of a _theory_. If you have a theory, you can answer any question about how the program relates to the world or how it should be changed — but you cannot completely communicate what's inside the black box, the theory, to someone else. No matter how clear your code is written, no matter now much documentation you provide — you still know something in your head that you haven't communicated to the rest of the team.

## Slide 30

Okay, one more analogy. I've often thought of dealing with programs like dealing with blueprints — except that you are only ever allowed to see a small fraction of the blueprint at any given time.

What do you think this blueprint is of?

…give time to think…

## Slide 31

That's right, it's a picture of the patent of the Enigma Machine from WWII. Obviously. No, I don't have any idea how it works.

This is kind of like how theories work. You are given glimpses, you make guesses, you build up a theory over time, and eventually you are able to connect all the little pieces together. This is much easier if you are the one who constructed the blueprint in the first place. Second best is having someone possessing the theory passing it on to you.

## Slide 32

This might all seem a bit philosophical. At the end of the day, does it really matter if this idea of "theory building" is an accurate assessment of the work of programming or not? I think there are quite a few very practical ways in which this can matter. As I said at the beginning, I'll cover just a few:

1. when to rewrite,
2. leading a team,
3. the usefulness of AI, and
4. compassion.

## Slide 33

Okay, starting with "when to rewrite."

## Slide 34

Naur uses the metaphor of life, death, and revival to describe what happens to a program as the people that _have_ the theory leave a team. He writes, "During the program _life_ a programmer team possessing its theory remains in active control of the program."

## Slide 35

"The _death_ of a program happens when the programmer team possessing its theory is dissolved."

## Slide 36

 A _dead_ program may continue to be used for execution in a computer and to produce useful results. The actual state of death becomes visible when demands for modifications of the program cannot be intelligently answered."

If Naur is correct about this theory-building being a central part of the programming task, then I think it gives one useful metric about whether you should rewrite: do you still have people with the theory on your team? If not — if the whole team has moved on — then you have a "dead" program. It is very possibly easier to rewrite and form a _new_ theory than it is to try re-build the theory of the existing program.

Naur would actually go further…

## Slide 37

 …[P]rogram revival, that is re-establishing the theory of a program merely from the documentation, is strictly impossible."

To be fair, we _do_ do this — maybe your program is only "mostly dead" — so Naur isn't fully correct here, and he does back off a bit from this claim later. But I think the general flavor of his point stands: trying to understand the theory of an existing program can be really hard, no matter how clear or well-documented it is.

## Slide 38

There are also management implications to this theory-building view. 

- How do you onboard new developers? Do you expect them to "read the code and go"? Do you expect them to "read the docs and go"? Or do you make sure they have access to someone who has the theory of the program?
- Are those with the theory prepared to share it with others? Do they mentor those without the theory? Is the time spent sharing the theory valued by you and the company?
- How do you maintain a theory across a large team? Do you recognize this as necessary time spent by your people in the course of a project?

Recognizing that there is a theory that must be transferred across the members of your team for them to perform most effectively changes how you work and collaborate.

## Slide 39

What about AI?

If the theory-building view is correct, what would Copilot or ChatGPT or whatever need to be an effective team member? It needs to have the _theory_ of the program and be able to communicate it to the programmers it is working with. It needs to explain the purpose of the program and its relation to the use-cases it is trying to solve, and how changes in the real-world problem ought to be handled in your program. Whether or not that day will (or has come) can be up for debate, but I think it is a useful yardstick to help accurately assess what LLMs are actually able to accomplish.

## Slide 40

Finally, and, I think, **most** importantly, this whole theory-building concept helps me have compassion, both for my team and for myself.

I'm reminded to have more compassion for a coworker who doesn't yet have the theory. I'm reminded to patiently teach them about the theory that I've already gained, and to encourage them to do the hard work of building their own theory.

I'm reminded to have more compassion for the _original_ programmer that wrote the code that I **hate** at the moment. I don't have the theory that they had — and maybe, if I had it, I'd find that their solution was much more elegant than I realized.

But I'm also reminded, when I'm stuck on a bit of code, that the process of theory-building is _hard_ work — and I need to have compassion with myself. I'm also reminded that I need to be willing to put in the work to _build_ a theory, rather than just making a change and hoping for the best.

I can't tell you whether Naur's theory-building view is 100% correct or mostly correct or maybe just fundamentally wrong. But I do find it a useful tool in thinking about the work of creating software — and maybe you will, too.

## Slide 41

Discussion!